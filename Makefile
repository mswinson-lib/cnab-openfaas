.PHONY: build deploy release install uninstall status

APPNAME ?= openfaas-swarm
VERSION ?= `cat VERSION`

DOCKER ?= docker
DOCKERAPP ?= docker-app
DOCKER_USER ?= $(error DOCKER_USER is undefined)
DOCKER_PASS ?= $(error DOCKER_PASS is undefined)
DOCKER_REPO ?= $(error DOCKER_REPO is undefined)

TAG_SUFFIX ?= '-develop'
TAG ?= $(VERSION)$(TAG_SUFFIX)

build:
	@$(DOCKERAPP) bundle --tag $(DOCKER_REPO)/$(APPNAME):$(TAG)

deploy:
	@$(DOCKER) login -u $(DOCKER_USER) -p $(DOCKER_PASS)
	@$(DOCKERAPP) push --tag $(DOCKER_REPO)/$(APPNAME):$(TAG)
	@$(DOCKERAPP) push --tag $(DOCKER_REPO)/$(APPNAME):latest-$(TAG_SUFFIX)

release:
	@$(DOCKER) login -u $(DOCKER_USER) -p $(DOCKER_PASS)
	@$(DOCKERAPP) push --tag $(DOCKER_REPO)/$(APPNAME):$(VERSION)
	@$(DOCKERAPP) push --tag $(DOCKER_REPO)/$(APPNAME):latest

install:
	@$(DOCKER) login -u $(DOCKER_USER) -p $(DOCKER_PASS)
	@$(DOCKERAPP) install $(DOCKER_REPO)/$(APPNAME):$(TAG) --name $(APPNAME)

uninstall:
	@$(DOCKERAPP) uninstall $(APPNAME)

status:
	@$(DOCKERAPP) status $(APPNAME)
