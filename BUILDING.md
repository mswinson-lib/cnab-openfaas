## Building

    git clone https://bitbucket.org/mswinson-lib/cnab-openfaas
    cd cnab-openfaas

    docker-compose run sandbox
    > export DOCKER_USER=<username>
    > export DOCKER_PASS=<dockerpass>
    > export DOCKER_REPO=<myrepo>

    > make build     # build CNAB bundle
    > make deploy    # push app snapshot to docker hub
    > make release   # push app to dockerhub as latest release
    > make install   # install CNAB bundle
    > make uninstall # uninstall CNAB bundle
    > make status    # display bundle status

